package main

import (
	"bufio"
	"fmt"
	"os"
	"os/exec"

	"gitlab.com/guingois/ugp/options"
	"gitlab.com/guingois/ugp/output"
	"gitlab.com/guingois/ugp/parser"
)

type PorcelainWriter interface {
	WriteV2([]byte) error
}

func gitIsAvailable() bool {
	cmd := exec.Command("git", "rev-parse", "--git-dir")
	return cmd.Run() == nil
}

func gitStatus(w PorcelainWriter) error {
	cmd := exec.Command("git", "status", "--branch", "--porcelain=v2")
	out, err := cmd.StdoutPipe()

	if err != nil {
		return err
	}

	if err := cmd.Start(); err != nil {
		return err
	}

	scanner := bufio.NewScanner(out)

	for scanner.Scan() {
		if err := w.WriteV2(scanner.Bytes()); err != nil {
			return err
		}
	}

	if err := scanner.Err(); err != nil {
		return err
	}

	if err := cmd.Wait(); err != nil {
		return err
	}

	return nil
}

func main() {
	if !gitIsAvailable() {
		fmt.Println()
		return
	}

	options := options.New(os.Args[1:])
	p := parser.New()

	if err := gitStatus(p); err != nil {
		panic(err)
	}

	status := p.Result()

	fmt.Println(output.New(status, options))
}
