package output

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestFindComponent0(t *testing.T) {
	s := "<prefix>"

	require.Equal(t, []int{0, 8}, findComponent(s))
}

func TestFindComponent1(t *testing.T) {
	s := "foo<prefixZ><foobar>"

	require.Equal(t, []int{12, 20}, findComponent(s))
}

func TestFindComponent2(t *testing.T) {
	s := "<>"

	require.Nil(t, findComponent(s))
}

func TestFindComponent3(t *testing.T) {
	s := "<foo<b>"

	require.Equal(t, []int{4, 7}, findComponent(s))
}
