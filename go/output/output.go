package output

import (
	"fmt"

	"gitlab.com/guingois/ugp/options"
	"gitlab.com/guingois/ugp/parser"
)

type Output struct {
	status  *parser.Result
	options *options.Options
}

func New(status *parser.Result, options *options.Options) Output {
	return Output{status, options}
}

func (o Output) Prefix() string {
	return o.options.Prefix + o.options.Reset
}

func (o Output) Suffix() string {
	return o.options.Suffix + o.options.Reset
}

func (o Output) Separator() string {
	return o.options.Separator + o.options.Reset
}

func (o Output) Clean() string {
	if o.status.IsClean() {
		return o.options.Clean + o.options.Reset
	} else {
		return ""
	}
}

func (o Output) Branch() string {
	return o.options.Branch + o.status.GetBranch() + o.options.Reset
}

func (o Output) Ahead() string {
	return o.count(o.options.Ahead, o.status.Ahead)
}

func (o Output) Behind() string {
	return o.count(o.options.Behind, o.status.Behind)
}

func (o Output) Index() string {
	return o.count(o.options.Index, o.status.Index)
}

func (o Output) Worktree() string {
	return o.count(o.options.Worktree, o.status.Worktree)
}

func (o Output) Unmerged() string {
	return o.count(o.options.Unmerged, o.status.Unmerged)
}

func (o Output) Untracked() string {
	return o.count(o.options.Untracked, o.status.Untracked)
}

func (o Output) count(prefix string, value int) string {
	if value == 0 {
		return ""
	} else {
		return fmt.Sprintf("%s%d%s", prefix, value, o.options.Reset)
	}
}

func (o Output) String() string {
	buf := ""
	tpl := o.options.Format

	for len(tpl) > 0 {
		loc := findComponent(tpl)

		if loc == nil {
			buf += tpl
			break
		}

		head := tpl[0:loc[0]]
		match := tpl[loc[0]:loc[1]]
		tail := tpl[loc[1]:]

		if len(head) != 0 {
			buf += head
		}

		switch match {
		case "<prefix>":
			buf += o.Prefix()
		case "<suffix>":
			buf += o.Suffix()
		case "<separator>":
			buf += o.Separator()
		case "<clean>":
			buf += o.Clean()
		case "<branch>":
			buf += o.Branch()
		case "<ahead>":
			buf += o.Ahead()
		case "<behind>":
			buf += o.Behind()
		case "<index>":
			buf += o.Index()
		case "<worktree>":
			buf += o.Worktree()
		case "<unmerged>":
			buf += o.Unmerged()
		case "<untracked>":
			buf += o.Untracked()
		default:
			buf += match
		}

		tpl = tail
	}

	return buf
}

// findComponent is an allocation-free alternative to:
// 	func findComponent(s string) []int {
// 		return regexp.MustCompile(`<[abcdefghijklmnopqrstuvwxyz]+>`).FindStringIndex(s)
// 	}
func findComponent(s string) []int {
	var leftIdx int
	var left, letter bool

	for i, c := range s {
		switch {
		case c == '<':
			leftIdx = i
			left = true

		case 'a' <= c && c <= 'z':
			letter = left

		case c == '>':
			if left && letter {
				return []int{leftIdx, i + 1}
			} else {
				left = false
				letter = false
			}

		default:
			left = false
			letter = false
		}
	}

	return nil
}
