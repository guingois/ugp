package options

import (
	"strings"
)

type Options struct {
	Format    string
	Reset     string
	Prefix    string
	Suffix    string
	Branch    string
	Ahead     string
	Behind    string
	Separator string
	Index     string
	Unmerged  string
	Worktree  string
	Untracked string
	Clean     string
}

func New(argv []string) *Options {
	o := Options{}

	max := len(argv)

	for i := 0; i < max; {
		if !strings.HasPrefix(argv[i], "--") {
			break
		}

		key := argv[i]
		val := ""

		j := i + 1

		if j < max && !strings.HasPrefix(argv[j], "--") {
			val = argv[j]
			i = j + 1
		} else {
			i = j
		}

		switch key {
		case "--format":
			o.Format = val
		case "--reset":
			o.Reset = val
		case "--prefix":
			o.Prefix = val
		case "--suffix":
			o.Suffix = val
		case "--branch":
			o.Branch = val
		case "--ahead":
			o.Ahead = val
		case "--behind":
			o.Behind = val
		case "--separator":
			o.Separator = val
		case "--index":
			o.Index = val
		case "--unmerged":
			o.Unmerged = val
		case "--worktree":
			o.Worktree = val
		case "--untracked":
			o.Untracked = val
		case "--clean":
			o.Clean = val
		}
	}

	return &o
}
