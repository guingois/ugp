function setup {
  mkdir local remote

  pushd remote
    git init --bare
  popd

  pushd local
    git init
    git remote add origin ../remote
    git commit --allow-empty --message commit_foo
    git push --quiet --set-upstream origin HEAD:foo
    git push --quiet --delete origin foo
  popd
}

function teardown {
  rm -rf local remote
}
