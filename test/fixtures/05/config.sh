function setup {
  mkdir local

  pushd local
    git init
    git commit --allow-empty --message commit_foo

    for i in $(seq 0 4)
    do
      echo foo > "file$i"
      git add "file$i"
    done
    git commit --message commit_foo

    git checkout --quiet -b other
    echo bar > file0
    echo bar > file1
    git commit --all --message commit_foo

    git checkout --quiet master
    echo baz > file0
    echo baz > file1
    git commit --all --message commit_foo

    set +e
    git merge --quiet other
    set -e

    echo baz > file2
    echo baz > file3
    echo baz > file4
    git add file{2,3,4}
  popd
}

function teardown {
  rm -rf local
}
