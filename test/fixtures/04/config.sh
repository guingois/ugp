function setup {
  mkdir local remote

  pushd remote
    git init --bare
  popd

  pushd local
    git init
    git remote add origin ../remote
    git commit --allow-empty --message commit_foo

    for i in $(seq 1 2)
    do
      echo $i > file0
      git add file0
      git commit --message commit_foo
    done

    git push --quiet --set-upstream origin master

    git reset --hard origin/master~2

    for i in $(seq 1 3)
    do
      echo $i > file0
      git add file0
      git commit --message commit_foo
    done
  popd
}

function teardown {
  rm -rf local remote
}
