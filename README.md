# UGP (Universal Git Prompt)

This project is a complete rewrite of [zsh-git-prompt][olivierverdier/zgp]
that aims to be much more performant and easy to maintain than the
original project.

At its core, UGP is a `git status` with a customizable output.

A Zsh integration is provided, but it should be easy to integrate UGP
with other shells.

## Why

[zsh-git-prompt][olivierverdier/zgp] provides a very informative prompt.
I used it successfully for a long time (thanks to its contributors !).

Now, the Python implementation is really slow and the Haskell one isn't
very convenient when you're not a Haskell developer. Plus, the project
appears to be unmaintained.

One day I tried to write my own implementation and noticed that it did
its job fast enough for the overhead to be unnoticeable (at least for
me). After a while I thought it could interest some people, so I cleaned
it up and created this repository.

The initial implementation was written in Ruby and is still maintained,
but you will probably prefer the go one, which is faster and is entirely
contained in its own executable.

## What

### Parts

From the prompt point of view, UGP has the same features as the original
[zsh-git-prompt][olivierverdier/zgp/examples]. It also fixes a few bugs
that happened on specific edge cases.

The core of UGP consists in an executable that analyzes the state of a
Git repository in the working directory and outputs its stats to the
standard output. The format can be customized with a set of switches
passed to the executable.

UGP core purposefully doesn't rely on shell code. Whatever can be done
without a shell should be done without a shell. The shell layer can
therefore be kept very small, and can easily be implemented in different
shell variants.

Thus, the rest of the tool is the shell layer that calls the executable
and injects its output into each command prompt. A default zsh
implementation is provided for convenience. Feel free to contribute to
this project by adding implementations for other shells.

The core is well tested, with a test suite running against fixtures of
Git repositories.

### Behavior

The executable exposes the following components:

name      | description
----------|------------
prefix    | the start of the output
suffix    | the end of the output
branch    | the branch name, or the abbreviated revision
behind    | the behind count for the branch relative to its upstream branch
ahead     | the ahead count for the branch relative to its upstream branch
separator | used to separate some components
index     | the staged files count
unmerged  | the files involved in conflicts count
worktree  | the modified files count
untracked | the untracked files count
clean     | used to indicate that index/unmerged/worktree/untracked are blank

If "count" components have a zero value, then they are excluded from the
output.

If any of `index`, `unmerged`, `worktree`, `untracked` have a non-zero
value, then `clean` is excluded from the output.

For example, for a repo on a branch up to date with its upstream
branch and only 3 staged files, the output would only include `prefix`,
`branch`, `separator`, `index` and `suffix`.

## Usage

The Ruby executable is located at `<path_to_ugp_repo>/ruby/ugp.rb`.
The Go executable is located at `<path_to_ugp_repo>/go/ugp` (you need to
build it first).

The executable accepts several switches:

switch      | description
------------|------------
--format    | the general structure of the output
--reset     | written at the end of each component (useful to reset terminal colors for example)
--prefix    | written at `prefix`
--suffix    | written at `suffix`
--branch    | written at the beginning of `branch`
--behind    | written at the beginning of `behind`
--ahead     | written at the beginning of `ahead`
--separator | written at `separator`
--index     | written at the beginning of `index`
--unmerged  | written at the beginning of `unmerged`
--worktree  | written at the beginning of `worktree`
--untracked | written at the beginning of `untracked`

For the `--format` switch, the value should be a list of `<component>`,
such as `<behind><separator><ahead><clean>`. You may insert arbitrary
characters between components (such as `=> REPO ON <branch> ! <=`. The
usual behavior regarding the `clean` or count components will still
apply. For example, on a unclean repo, `The repo is <clean>` would
output `The repo is `.

### zsh

Once you've cloned the repository, simply add the following somewhere in
a zsh startup file:

```shell
source <path_to_ugp_repo>/zshrc.sh

# the important part is: $(ugp_display)
PS1='this is my prompt, and here is the git status: $(ugp_display) !'
```

A set of global variables controls the call to the executable. All of
them have a default value that you can override or even unset once
you've sourced the `zshrc.sh` file.

The executable switches are exposed with `UGP_<switch>` variables, such
as `UGP_RESET` being the equivalent of `--reset`.

The executable path is controled by the `UGP_EXECUTABLE` variable. This
variable cannot be blank and will always default to the path of the Ruby
executable.

## Miscellaneous

I wasn't aware of any similar project when I first wrote my own
implementation.

Things have changed now, there are some alternatives:

- If you care about the original project codebase, you might be
    interested in a [friendly fork][starcraftman/zgp] of the original
    project. The author aims at maintaining the original codebase and
    adding new features.
- If you want to stay as close to Zsh as possible, have a look at
    [this clever rewrite][woefe/git-prompt.zsh].
- A [very simple alternative][Streetwalrus/gitprompt-rs] written in Rust
    also exists.

[olivierverdier/zgp]: https://github.com/olivierverdier/zsh-git-prompt
[olivierverdier/zgp/examples]:https://github.com/olivierverdier/zsh-git-prompt#examples
[starcraftman/zgp]: https://github.com/starcraftman/zsh-git-prompt
[woefe/git-prompt.zsh]: https://github.com/woefe/git-prompt.zsh
[Streetwalrus/gitprompt-rs]: https://github.com/Streetwalrus/gitprompt-rs

## License

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or any later
version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>.
