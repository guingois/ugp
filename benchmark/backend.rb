# frozen_string_literal: true

require_relative "lib/bm"
require "shellwords"

class Backend
  EXE = ["ruby/ugp.rb"].freeze

  format = %w[
    <prefix>
    <branch>
    <behind>
    <ahead>
    <separator>
    <index>
    <unmerged>
    <worktree>
    <untracked>
    <clean>
    <suffix>
  ].join

  FLAGS = {
    "--format"    => format,
    "--reset"     => "∅",
    "--prefix"    => "(",
    "--suffix"    => ")",
    "--branch"    => "≡",
    "--behind"    => "↓",
    "--ahead"     => "↑",
    "--separator" => "|",
    "--index"     => "●",
    "--unmerged"  => "✖",
    "--worktree"  => "✚",
    "--untracked" => "∗",
    "--clean"     => "✔"
  }.flatten.freeze

  def initialize(exe = nil)
    @cmd = build_cmd(exe)
    @env = build_env
    @opts = build_opts
  end

  def call(env = nil)
    env = env ? @env.merge(env) : @env
    system(env, *@cmd, **@opts)
  end

  def to_s
    Shellwords.join(@cmd)
  end

  private

  def build_cmd(exe)
    exe = EXE if exe.nil? || exe.empty?
    cmd = exe + FLAGS
    cmd[0] = File.expand_path(cmd[0]) if cmd[0].include?("/")
    cmd.freeze
  end

  def build_env
    { "PATH" => "#{File.expand_path("bin", __dir__)}:#{ENV["PATH"]}" }.freeze
  end

  def build_opts
    { %i[out err].freeze => :close, exception: true }.freeze
  end
end

class Task
  def initialize
    @backend = Backend.new(ARGV)
  end

  def run
    @backend.call("FAKE_DELAY" => "0")
  end

  def label
    @backend.to_s
  end

  def store
    "backend"
  end
end

task = Task.new
iterations = 20

BM.call(task, iterations)
